import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountsService } from 'src/app/shared/services/accounts.service';
import { SessionsService } from 'src/app/shared/services/sessions.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  isVisible: boolean = false

  constructor(
    private accountSvc: AccountsService,
    private sessionSvc: SessionsService,
    private router: Router) {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {
  }

  login() {
    this.accountSvc
      .authenticate(
        this.loginForm.get('email')?.value,
        this.loginForm.get('password')?.value
      )
      .subscribe((e) => {
        this.sessionSvc.setObject('userinfo', e)
        this.router.navigateByUrl('/app')
      })
  }

}
