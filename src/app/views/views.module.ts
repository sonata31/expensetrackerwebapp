import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularuiModule } from 'src/app/shared/ui/angularui.module';

import { LoginComponent } from './login/login.component';
import { Client } from '../shared/services/apiclient';

@NgModule({
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AngularuiModule
  ],
  providers: [
    Client
  ]
})
export class ViewsModule { }
