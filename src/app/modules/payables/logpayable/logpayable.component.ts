import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-logpayable',
  templateUrl: './logpayable.component.html',
  styleUrls: ['./logpayable.component.scss']
})
export class LogpayableComponent implements OnInit {
  items: string[] = []
  months: string[] = []
  years: number[] = []
  selectedItem: string = ''
  logPayableForm: FormGroup

  constructor() {
    this.logPayableForm = new FormGroup({
      payable: new FormControl('', Validators.required),
      payableFromMonth1: new FormControl(''),
      payableToYear1: new FormControl(''),
      payableFromMonth2: new FormControl(''),
      payableToYear2: new FormControl(''),
      amount: new FormControl('', Validators.required),
      modeOfPayment: new FormControl('', Validators.required),
      referenceNo: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {
    this.loadItems()
    this.loadMonths()
    this.loadYear()
  }

  logPayable() {
    console.log(this.logPayableForm.value)
  }

  loadItems() {
    this.items = ['Rent', 'Internet', 'SSS', 'Pag-Ibig']
  }

  loadMonths() {
    this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  }

  loadYear() {
    this.years = [2028, 2027, 2026, 2025, 2024, 2023, 2022, 2021, 2020, 2019]
  }
}
