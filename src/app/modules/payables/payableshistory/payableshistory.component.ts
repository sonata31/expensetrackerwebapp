import { Component, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { PayableshistorydatagridService } from 'src/app/shared/services/payableshistorydatagrid.service';

@Component({
  selector: 'app-payableshistory',
  templateUrl: './payableshistory.component.html',
  styleUrls: ['./payableshistory.component.scss']
})
export class PayableshistoryComponent implements OnInit {
  colDefs: ColDef[] = []
  data: any[] = []

  constructor(private gridSvc: PayableshistorydatagridService) { }

  ngOnInit(): void {
    this.colDefs = this.gridSvc.getColumnDefinitions()
    this.data = this.gridSvc.getDummyRowData()
  }
}
