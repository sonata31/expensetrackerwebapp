import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddpayableComponent } from './addpayable/addpayable.component';
import { LogpayableComponent } from './logpayable/logpayable.component';
import { PayableshistoryComponent } from './payableshistory/payableshistory.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        component: AddpayableComponent
      },
      {
        path: 'log',
        component: LogpayableComponent
      },
      {
        path: 'history',
        component: PayableshistoryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayablesRoutingModule { }
