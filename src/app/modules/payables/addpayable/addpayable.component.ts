import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-addpayable',
  templateUrl: './addpayable.component.html',
  styleUrls: ['./addpayable.component.scss']
})
export class AddpayableComponent implements OnInit {
  payableForm: FormGroup

  constructor() {
    this.payableForm = new FormGroup({
      name: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {
  }
}
