import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AngularuiModule } from 'src/app/shared/ui/angularui.module';
import { PayablesRoutingModule } from './payables-routing.module';

import { AddpayableComponent } from './addpayable/addpayable.component';
import { LogpayableComponent } from './logpayable/logpayable.component';
import { PayableshistoryComponent } from './payableshistory/payableshistory.component';
import { MainComponent } from './main/main.component';
import { DatagridModule } from 'src/app/shared/components/grid/datagrid/datagrid.module';

@NgModule({
  declarations: [
    AddpayableComponent,
    LogpayableComponent,
    PayableshistoryComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PayablesRoutingModule,
    AngularuiModule,
    DatagridModule
  ]
})
export class PayablesModule { }
