import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Client } from 'src/app/services/apiclient';

@Component({
  selector: 'app-logsavings',
  templateUrl: './logsavings.component.html',
  styleUrls: ['./logsavings.component.scss']
})
export class LogsavingsComponent implements OnInit {
  savingsForm: FormGroup

  constructor(private api: Client) {
    this.savingsForm = new FormGroup({
      amount: new FormControl('', Validators.required),
      depositDate: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {
  }

  submit() {
    this.api
      .apiV1SavingsPost({
        amount: this.savingsForm.get('amount')?.value,
        depositDate: this.savingsForm.get('depositDate')?.value,
        userId: 1
      })
      .subscribe(
        (e) => console.log(e),
        (err) => console.log(err)
      )
  }
}
