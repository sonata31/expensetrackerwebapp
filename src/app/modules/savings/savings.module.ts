import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SavingsRoutingModule } from './savings-routing.module';
import { LogsavingsComponent } from './logsavings/logsavings.component';
import { AngularuiModule } from 'src/app/shared/ui/angularui.module';
import { DatagridModule } from 'src/app/shared/components/grid/datagrid/datagrid.module';
import { UiModule } from 'src/app/shared/ui/ui.module';
import { HttpClientModule } from '@angular/common/http';
import { Client } from 'src/app/services/apiclient';
import { SavingshistoryComponent } from './savingshistory/savingshistory.component';

@NgModule({
  declarations: [
    LogsavingsComponent,
    SavingshistoryComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    SavingsRoutingModule,
    ReactiveFormsModule,
    AngularuiModule,
    DatagridModule,
    UiModule
  ],
  providers: [Client]
})
export class SavingsModule { }
