import { Component, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { SavingshistorydatagridService } from 'src/app/shared/services/savingshistorydatagrid.service';

@Component({
  selector: 'app-savingshistory',
  templateUrl: './savingshistory.component.html',
  styleUrls: ['./savingshistory.component.scss']
})
export class SavingshistoryComponent implements OnInit {
  colDefs: ColDef[] = []
  data: any[] = []

  constructor(private gridSvc: SavingshistorydatagridService) { }

  ngOnInit(): void {
    this.colDefs = this.gridSvc.getColumnDefinitions()
    this.data = this.gridSvc.getDummyRowData()
  }

}
