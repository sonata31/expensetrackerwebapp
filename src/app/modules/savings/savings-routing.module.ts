import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogsavingsComponent } from './logsavings/logsavings.component';
import { SavingshistoryComponent } from './savingshistory/savingshistory.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'log',
        component: LogsavingsComponent
      },
      {
        path: 'history',
        component: SavingshistoryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SavingsRoutingModule { }
