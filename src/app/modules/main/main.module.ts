import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UiModule } from 'src/app/shared/ui/ui.module';
import { AngularuiModule } from 'src/app/shared/ui/angularui.module';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    UiModule,
    AngularuiModule
  ]
})
export class MainModule { }
