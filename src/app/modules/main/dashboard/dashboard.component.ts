import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbMenuItem } from '@nebular/theme';

import { NavigationService } from 'src/app/shared/services/navigation.service';
import { SessionsService } from 'src/app/shared/services/sessions.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  items: NbMenuItem[] = []

  constructor(
    private navSvc: NavigationService,
    private sessionSvc: SessionsService,
    private router: Router) { }

  ngOnInit(): void {
    this.items = this.navSvc.getLinks()
  }

  logout() {
    this.sessionSvc.clearItems()
    this.router.navigateByUrl('/login')
  }

}
