import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'payables',
        loadChildren: () => import('../payables/payables.module').then(m => m.PayablesModule)
      },
      {
        path: 'savings',
        loadChildren: () => import('../savings/savings.module').then(m => m.SavingsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
