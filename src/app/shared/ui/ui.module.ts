import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbDatepickerModule, NbLayoutModule, NbMenuModule, NbSidebarModule } from '@nebular/theme';

@NgModule({
  imports: [
    CommonModule,
    NbMenuModule,
    NbCardModule,
    NbLayoutModule,
    NbSidebarModule,
    NbDatepickerModule
  ],
  exports: [
    NbMenuModule,
    NbCardModule,
    NbLayoutModule,
    NbSidebarModule,
    NbDatepickerModule
  ]
})
export class UiModule { }
