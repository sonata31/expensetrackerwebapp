import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatagridComponent } from './datagrid.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    DatagridComponent
  ],
  exports: [
    DatagridComponent
  ],
  imports: [
    CommonModule,
    AgGridModule.withComponents([])
  ]
})
export class DatagridModule { }
