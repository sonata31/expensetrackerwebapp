import { Component, Input, OnInit } from '@angular/core';
import { ColDef, GridOptions } from 'ag-grid-community';

@Component({
  selector: 'app-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.scss']
})
export class DatagridComponent implements OnInit {
  @Input() columnDefs: ColDef[] = []
  @Input() rowData: any[] = []

  constructor() {
  }

  ngOnInit(): void {
  }
}
