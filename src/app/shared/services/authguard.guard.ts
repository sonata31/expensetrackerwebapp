import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenModel } from './apiclient';
import { SessionsService } from './sessions.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {
  constructor(
    private sessionSvc: SessionsService,
    private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let currUser = <TokenModel> this.sessionSvc.getObject('userinfo')
      
      if (currUser !== null) return true
      else {
        this.router.navigateByUrl('/login')
        return false
      }
  }
}
