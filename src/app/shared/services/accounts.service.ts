import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client, TokenModel } from './apiclient';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  constructor(private client: Client) { }

  authenticate(email: string, password: string): Observable<TokenModel> {
    return this.client.apiV1AccountLogin({email: email, password: password})
  }
}
