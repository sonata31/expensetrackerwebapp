import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionsService {
  constructor() { }

  setObject(key: string, obj: any) {
    let data = JSON.stringify(obj)
    localStorage.setItem(key, data)
  }

  getObject(key: string) {
    let data: any = localStorage.getItem(key)
    return JSON.parse(data)
  }

  setItem(key: string, value: string) {
    localStorage.setItem(key, value)
  }

  getItem(key: string) {
    return localStorage.getItem(key)
  }

  clearItems() {
    localStorage.clear()
  }
}
