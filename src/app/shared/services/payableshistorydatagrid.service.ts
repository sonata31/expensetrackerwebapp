import { Injectable } from '@angular/core';
import { ColDef } from 'ag-grid-community';

@Injectable({
  providedIn: 'root'
})
export class PayableshistorydatagridService {
  constructor() { }

  getColumnDefinitions(): ColDef[] {
    return <ColDef[]> [
      {
        headerName: 'Payable',
        field: 'paidFor',
        sortable: true,
        filter: true
      },
      {
        headerName: 'Amount',
        field: 'amount',
        filter: true
      },
      {
        headerName: 'Coverage From',
        field: 'covFrom',
        sortable: true,
        filter: true
      },
      {
        headerName: 'Coverage To',
        field: 'covTo',
        sortable: true,
        filter: true
      },
      {
        headerName: 'Date Paid',
        field: 'datePaid',
        sortable: true,
        filter: true
      }
    ]
  }

  getDummyRowData() {
    return [
      { paidFor: 'Test', amount: 1000, covFrom: '01/01/2020', covTo: '01/02/2020', datePaid: '01/01/2020' },
      { paidFor: 'Test', amount: 1000, covFrom: '01/01/2020', covTo: '01/02/2020', datePaid: '01/01/2020' },
      { paidFor: 'Test', amount: 1000, covFrom: '01/01/2020', covTo: '01/02/2020', datePaid: '01/01/2020' },
      { paidFor: 'Test', amount: 1000, covFrom: '01/01/2020', covTo: '01/02/2020', datePaid: '01/01/2020' },
    ]
  }
}
