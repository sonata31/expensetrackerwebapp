import { Injectable } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  constructor() { }

  getLinks(): NbMenuItem[] {
    return <NbMenuItem[]> [
      {
        title: 'Payables',
        children: [
          {
            title: 'Add Payable',
            link: '/app/payables/add'
          },
          {
            title: 'Log Payable',
            link: '/app/payables/log'
          },
          {
            title: 'View History',
            link: '/app/payables/history'
          },
        ],
      },
      {
        title: 'Savings',
        children: [
          {
            title: 'Log Savings',
            link: '/app/savings/log'
          },
          {
            title: 'View History',
            link: '/app/savings/history'
          }
        ],
      }
    ];
  }
}
