import { Injectable } from '@angular/core';
import { ColDef } from 'ag-grid-community';

@Injectable({
  providedIn: 'root'
})
export class SavingshistorydatagridService {
  constructor() { }

  getColumnDefinitions(): ColDef[] {
    return <ColDef[]> [
      {
        headerName: 'Amount',
        field: 'amount',
        sortable: true,
        filter: true
      },
      {
        headerName: 'Deposit Date',
        field: 'depositDate',
        filter: true
      }
    ]
  }

  getDummyRowData() {
    return [
      { amount: 1000, depositDate: '01/01/2020' },
      { amount: 1000, depositDate: '01/01/2020' },
      { amount: 1000, depositDate: '01/01/2020' },
      { amount: 1000, depositDate: '01/01/2020' },
    ]
  }
}
